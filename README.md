# D7Security Gitlab components

Gitlab CI components to create Drupal 7 module release notes and tarballs automatically when a git tag is pushed to a project.

Check the [D7Security wiki](https://gitlab.com/d7security/d7security/-/wikis/home) for more documentation.
